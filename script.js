
		function calculate() {
			var a = parseFloat(document.getElementById("num1.1").value);
			var b = parseFloat(document.getElementById("num1.2").value);
			var c = parseFloat(document.getElementById("num1.3").value);

			if (isNaN(a) || isNaN(b) || isNaN(c)) {
			  document.getElementById("result_calk").innerHTML = "Помилка: введено недійсне значення!";
			} else if (a === b || b === c || a === c) {
			  document.getElementById("result_calk").innerHTML = "Помилка: є рівні числа!";
			} else {
			  let average = (a + b + c) / 3;
			  document.getElementById("result_calk").innerHTML = "Середнє арифметичне: " + average;
			}
		}

		function findMax() {
			var a = parseFloat(document.getElementById("max.num").value);
			var b = parseFloat(document.getElementById("max.num2").value);
			var c = parseFloat(document.getElementById("max.num3").value);

			if (isNaN(a) || isNaN(b) || isNaN(c)) {
			  alert("Помилка: введено недійсне значення!");
			} else {
			  var max = a;
			  if (b > max) {
			    max = b;
			  }
			  if (c > max) {
			    max = c;
			  }
			  document.getElementById("resultmax").innerHTML = "Максимальне число: " + max;
			}
		}

		function sumRange() {
  var numA = document.getElementById("numA").value;
  var numB = document.getElementById("numB").value;
  var result = document.getElementById("result_sum1");
  

  if (numA === "" || numB === "" || numA >= numB) {
    result.innerHTML = "неправильно введені дані";
    return;
  }
  

  var sum = 0;
  for (var i = numA; i <= numB; i++) {
    sum += i;
  }
  
  result.innerHTML = `Сумма чисел від ${numA} до ${numB} рівно ${sum}`;
}

function printOddNumbers(A, B) {
  var numA = document.getElementById("numA").value;
  var numB = document.getElementById("numB").value;
  var result = document.getElementById("result_sum2");
  for (let i = A; i <= B; i++) {
    if (i % 2 !== 0) {
      console.log(i);
    }
  }
   result.innerHTML = "Максимальне число з непарних: " + max;

}

function sumRange() {
  var n = document.getElementById("numF1").value;
  var result = "";

  function fibonacci(n) {
    if (n <= 1) {
      return n;
    } else {
      return fibonacci(n - 1) + fibonacci(n - 2);
    }
  }
    
  for (var i = 0; i < n; i++) {
    result += fibonacci(i) + " ";
  }

  document.getElementById("resultfib").innerHTML = result;
}
